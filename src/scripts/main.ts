
declare const M: any;

document.addEventListener('DOMContentLoaded', function() {
    var caruselContainer = document.querySelector('.carousel.clients');
    M.Carousel.init(caruselContainer, {
        fullWidth: true,
        indicators: true
    });
});
