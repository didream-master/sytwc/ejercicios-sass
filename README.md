### Ejercicios Sass
El propósito del presente proyecto es realizar un página web que use SCSS para implementar los estilos y cumpla los requisitos comentados en [enunciado.pdf](enunciado.pdf).

Como proyecto base se utiliza el proyecto del repositorio https://gitlab.com/didream-master/sytwc/generacion-proyecto-automatizacion-tareas.


### _variable.scss
Se ha creado el fichero [src/styles/_variables.scss](src/styles/_variables.scss).

### _mixins.scss
Se ha creado el fichero [src/styles/_mixins.scss](src/styles/_mixins.scss).

### _base.scss. Estilos comunes, aplicables a todas las páginas del sitio.
Fichero [src/styles/_base.scss](src/styles/_base.scss)


### mixins para los encabezados, h1
```scss
@mixin title1 {
    font-size: 2rem;
    font-weight: normal;
    margin: 0;
}
```


### mixin para centrar el contenido
Se ha creado un mixin haciendo uso de la propiedad `text-align`:
```scss
@mixin center-text {
    text-align: center;
}
```
útil para centrar texto, por ejemplo.

Y otro para centrar contenido haciendo uso de flexbox:
```scss
@mixin center-content {
    display: flex;
    justify-content: center;
}
```


### mixin para estilos del form
```scss
@mixin base-form {
    display: block;
    .form-group {
        margin-bottom: .5rem;
        label {
            display: block;
            margin-bottom: .5rem;
        }
        .form-control {
            height: $form-control-height;
            border: 1px solid $form-control-border-color;
            color: #222B46;
            border-radius: 0.25rem;
            outline: none;
            box-shadow: none;
            display: block;
            width: 100%;
            padding: .6rem;
            font-size: 1rem;
            font-family: inherit;
            background-color: #f7f9fc;
            &::placeholder {
                color: #a7b0c4;
            }

            // ENUNCIADO: Anidar los estilos de los elementos dentro del form, para que se resalten cuando tenga el foco
            &:focus {
                background-color: white;
            }
            
        }
        textarea.form-control {
            height: auto;
            min-height: $form-control-height;
            resize: vertical;
        }
    }
    .form-actions {
        margin-top: .75rem;
        .form-action {
            background-color: #00d68f;
            border-color: #00d68f;
            color: white;

            // ENUNCIADO: Anidar los estilos de los botones del form, para que se apliquen estilos específicos en el hover de ellos
            // ENUNCIADO: Anidar los estilos de los elementos dentro del form, para que se resalten cuando tenga el foco
            &:hover, &:focus {
                background-color: #00b887;
                border-color: #00997a;
            }
        }
    }
}
```

### Anidar los estilos de los botones del form, para que se apliquen estilos específicos en el hover de ellos
```scss
@mixin base-form {
    //...
    .form-actions {
        //...
        .form-action {
            //...
            &:hover, &:focus {
                background-color: #00b887;
                border-color: #00997a;
            }
        }
    }
}
```

### Anidar los estilos de los elementos dentro del form, para que se resalten cuando tenga el foco
```scss
@mixin base-form {
    //...
    .form-group {
        //...
        .form-control {
            //...
            &:focus {
                background-color: white;
            }
            
        }
    }
    .form-actions {
        //...
        .form-action {
            //...
            &:hover, &:focus {
                background-color: #00b887;
                border-color: #00997a;
            }
        }
    }
}
```


### Utilizados el mismo estilo en los encabezados de la parte de testimonio que en las garantías, pero con un cambio en el color de texto
```scss
.section-testimonios {
    // ...
    .section-title {
        color: white;
    }
}
```

### Utilizar algun estilo de materialize sin incluir todos los estilos. Reutiliza sólo algún componente
Para incorporar Materialize se ha añadido su código SCSS a [src/styles/vendor/materialize](src/styles/vendor/materialize) y el js en [src/scripts/vendor/materialize](src/scripts/vendor/materialize). A su vez, en [index.html](index.html) se ha añadido la ruta del código resultante (css y js) tras ejecutarse gulp.

Se han incluido dos componentes de Materialize; por un lado se ha incorporado el componente `grid` para crear la página web responsive, y por otro, el componente `carusel`, el cual se usa en la sección de testimonios de clientes.
