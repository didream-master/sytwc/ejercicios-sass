const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const imagemin = require('gulp-imagemin');
const eventStream = require('event-stream');

const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');

function clean() {
    return del([ 'dist' ]);
}

function styleSass() {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
}

function imageCompress() {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
}

function parseJavascript() {
    return eventStream.concat(
        tsProject.src().pipe(tsProject()).js,
        gulp.src('src/scripts/**/*.js')
    )
        .pipe(gulp.dest('dist/scripts'))
        .pipe(browserSync.stream());
}

function serve() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('./src/**/*.scss', styleSass);
    gulp.watch("./**/*.html").on('change', browserSync.reload);
    gulp.watch('./src/**/*.ts', parseJavascript);
}
const build = gulp.series(clean, gulp.parallel(styleSass, parseJavascript, imageCompress));

exports.build = build;
exports.default = gulp.series(build, serve);